package Main;

import javafx.animation.RotateTransition;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Pane;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.util.Optional;

/**
 * Created by Владислав on 16.05.2016.
 */
public class Character extends Pane {

    ImageView imageView;
    int width = 20;
    int height = 20;
    String orientation = "UP";

    public Character(ImageView imageView) {
        this.imageView = imageView;
        imageView.setFitWidth(width);
        imageView.setFitHeight(height);
        this.setTranslateX(100);
        this.setTranslateY(100);
        getChildren().addAll(imageView);
    }

    public void setOrienationRight() {
        RotateTransition rt = new RotateTransition(Duration.millis(100), this.imageView);
        if (orientation.contentEquals("UP")) {
            rt.setByAngle(90);
            orientation = "RIGHT";
            rt.play();
        }
        if (orientation.contentEquals("LEFT")) {
            rt.setByAngle(180);
            orientation = "RIGHT";
            rt.play();
        }
        if (orientation.contentEquals("DOWN")) {
            rt.setByAngle(-90);
            orientation = "RIGHT";
            rt.play();
        }
    }

    public void setOrienationLeft() {
        RotateTransition rt = new RotateTransition(Duration.millis(100), this.imageView);
        if (orientation.contentEquals("UP")) {
            rt.setByAngle(-90);
            orientation = "LEFT";
            rt.play();
        }
        if (orientation.contentEquals("RIGHT")) {
            rt.setByAngle(180);
            orientation = "LEFT";
            rt.play();
        }
        if (orientation.contentEquals("DOWN")) {
            rt.setByAngle(90);
            orientation = "LEFT";
            rt.play();
        }
    }

    public void setOrienationUp() {
        RotateTransition rt = new RotateTransition(Duration.millis(100), this.imageView);
        if (orientation.contentEquals("LEFT")) {
            rt.setByAngle(90);
            orientation = "UP";
            rt.play();
        }
        if (orientation.contentEquals("RIGHT")) {
            rt.setByAngle(-90);
            orientation = "UP";
            rt.play();
        }
        if (orientation.contentEquals("DOWN")) {
            rt.setByAngle(180);
            orientation = "UP";
            rt.play();
        }
    }

    public void setOrienationDown() {
        RotateTransition rt = new RotateTransition(Duration.millis(100), this.imageView);
        if (orientation.contentEquals("UP")) {
            rt.setByAngle(180);
            orientation = "DOWN";
            rt.play();
        }
        if (orientation.contentEquals("RIGHT")) {
            rt.setByAngle(90);
            orientation = "DOWN";
            rt.play();
        }
        if (orientation.contentEquals("LEFT")) {
            rt.setByAngle(-90);
            orientation = "DOWN";
            rt.play();
        }
    }

    //движения по горизонтали и обработка столкновения с препятствиями
    public void moveX(int x) {
        boolean right = x > 0 ? true : false;
        for (int i = 0; i < Math.abs(x); i++) {

            if (right) {
                this.setTranslateX(this.getTranslateX() + 1);
                setOrienationRight();
            } else {
                this.setTranslateX(this.getTranslateX() - 1);
                setOrienationLeft();

            }

            for (Wall w : Main.walls) {
                if (this.getBoundsInParent().intersects(w.getBoundsInParent())) {
                    if (right) {
                        if (getTranslateX() + 19 == w.getTranslateX())
                            setTranslateX(getTranslateX() - 1);
                    } else {

                        if (getTranslateX() - 4 == w.getTranslateX())
                            setTranslateX(getTranslateX() + 1);
                    }
                    return;


                }
            }
            if (getTranslateX() < 0) {
                setTranslateX(0);
            }
            if (getTranslateX() > 750) {
                setTranslateX(750);
            }

            if (this.getBoundsInParent().intersects(Main.end.getBoundsInParent())) {
                if (right) {
                    if (getTranslateX() + 5 == Main.end.getTranslateX()) {
//                        Main.end.setOpacity(0.5);
                        ModalWindow.newWindow("Victory");
//                        TextInputDialog dialog = new TextInputDialog("walter");
//                        dialog.setTitle("Text Input Dialog");
////                        dialog.setHeaderText("Look, a Text Input Dialog");
//                        dialog.setContentText("Please enter your name:");
//
//// Traditional way to get the response value.
//                        dialog.show();
//                        String result = dialog.getContentText();
//                        System.out.println("Your name: " + result);
                    }
                }
            }
            setTranslateX(getTranslateX() + (right ? 1 : -1));
        }


    }

    //движения по вертикали и обработка столкновения с препятствиями
    public void moveY(int y) {
        boolean down = y > 0 ? true : false;
        for (int i = 0; i < Math.abs(y); i++) {

            if (down) {
                this.setTranslateY(this.getTranslateY() + 1);
                setOrienationDown();
            } else {
                this.setTranslateY(this.getTranslateY() - 1);
                setOrienationUp();
            }

            for (Wall w : Main.walls) {
                if (this.getBoundsInParent().intersects(w.getBoundsInParent())) {
                    if (getTranslateY() + 19 == w.getTranslateY())
                        setTranslateY(getTranslateY() - 1);
                    if (getTranslateY() - 39 == w.getTranslateY())
                        setTranslateY(getTranslateY() + 1);
                    if (getTranslateY() - 4 == w.getTranslateY())
                        setTranslateY(getTranslateY() + 1);
                    return;
                }

            }
        }
        if (getTranslateY() < 0) {
            setTranslateY(0);
        }
        if (getTranslateY() > 600) {
            setTranslateY(600);
        }
        setTranslateY(getTranslateY() + (down ? 1 : -1));
    }
}
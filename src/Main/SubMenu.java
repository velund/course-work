package Main;

import javafx.scene.layout.VBox;

/**
 * Created by Владислав on 16.05.2016.
 */
//Класс добавления кнопок в один бокс
public class SubMenu extends VBox {
    public SubMenu(MenuItem... items) {
        setSpacing(15);
        setTranslateX(200);
        setTranslateY(100);
        for (MenuItem item :
                items) {
            getChildren().addAll(item);
        }
    }
}

package Main;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Владислав on 21.05.2016.
 */
public class Wall extends Pane {
    Rectangle rect;
    int height;
    int width;

    public Wall(int height, int width) {
        this.width = width;
        this.height = height;
        rect = new Rectangle(height, width, Color.BLACK);


        getChildren().add(rect);
    }
}

package Main;

import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * Created by Владислав on 16.05.2016.
 */
//Класс кнопок с анимацией
public class MenuItem extends StackPane{
    public MenuItem(String name) {
        Rectangle rt = new Rectangle(400, 70, Color.DARKGRAY);
        rt.setOpacity(0.5);

        Text text = new Text(name);
        text.setFill(Color.WHITE);
        text.setFont(Font.font("Arial", FontWeight.BOLD, 16));

        setAlignment(Pos.CENTER);
        getChildren().addAll(rt, text);
        FillTransition st = new FillTransition(Duration.seconds(0.5), rt);
        setOnMouseEntered(event -> {
            st.setFromValue(Color.DARKGRAY);
            st.setToValue(Color.DARKGOLDENROD);
            st.setCycleCount(Animation.INDEFINITE);
            st.setAutoReverse(true);
            st.play();
        });
        setOnMouseExited(event -> {
            st.stop();
            rt.setFill(Color.DARKGRAY);
        });

    }
}

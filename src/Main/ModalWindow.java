package Main;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;

/**
 * Created by Владислав on 23.06.2016.
 */
public class ModalWindow {

    public static void newWindow(String title) {

        Stage window = new Stage();
//        window.initModality(Modality.APPLICATION_MODAL);
        Pane pane = new Pane();

        Button btn = new Button("OK");
        btn.setTranslateX(85);
        btn.setTranslateY(60);

        Label label = new Label();
        label.setTranslateY(40);
        label.setTranslateX(20);


        TextField name = new TextField();
        name.setPromptText("Enter your name");
        name.setTranslateX(30);
        name.setTranslateY(10);


        btn.setOnMouseClicked(event -> {
            if (name.getText().isEmpty()) {
                label.setText("You have not entered your name");
            } else {
                window.close();
                System.out.println(name.getText());
            }
        });

        pane.getChildren().addAll(btn, name, label);
        Scene scene = new Scene(pane, 200, 100);
        window.setScene(scene);
        window.setTitle(title);
        window.show();
    }

}

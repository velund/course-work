package Main;

import javafx.scene.layout.Pane;

/**
 * Created by Владислав on 16.05.2016.
 */
public class MenuBox extends Pane {
    static SubMenu subMenu;

    public MenuBox(SubMenu subMenu) {
        MenuBox.subMenu = subMenu;
        setVisible(true);

        getChildren().addAll(subMenu);
    }

    public void setSubMenu(SubMenu subMenu) {
        getChildren().remove(MenuBox.subMenu);
        MenuBox.subMenu = subMenu;
        getChildren().add(MenuBox.subMenu);
    }
}

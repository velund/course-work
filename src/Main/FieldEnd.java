package Main;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Владислав on 23.06.2016.
 */
public class FieldEnd extends Wall {
    Rectangle rect;

    public FieldEnd(int height, int width) {
        super(height, width);
        rect = new Rectangle(height, width, Color.DARKGREEN);
        rect.setOpacity(0.5);

        getChildren().add(rect);
    }
}

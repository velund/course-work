package Main;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Владислав on 04.05.2016.
 */
public class Main extends Application {

    private HashMap<KeyCode, Boolean> keys = new HashMap<>();
    Image image = new Image(getClass().getResourceAsStream("icon.png"));
    ImageView imageView = new ImageView(image);
    Character player = new Character(imageView);

    public static FieldEnd end = new FieldEnd(20, 500);

    //Рабочия панель
    Pane root = new Pane();
    Pane reyting = new Pane();
    public static Pane game = new Pane();

    public static ArrayList<Wall> walls = new ArrayList<>();


    public boolean isPressed(KeyCode keyCode) {
        return keys.getOrDefault(keyCode, false);
    }

    //обработчик движения героя
    public void update() {
        if (isPressed(KeyCode.UP)) {
            player.moveY(-1);
        } else if (isPressed(KeyCode.DOWN)) {
            player.moveY(1);
        } else if (isPressed(KeyCode.RIGHT)) {
            player.moveX(1);
        } else if (isPressed(KeyCode.LEFT)) {
            player.moveX(-1);
        }

    }

    int x = 200;
    int y = 10;

    public Parent genLabirint(int hr) {
        for (int i = 0; i < hr + 1; i++) {
            x = 200;
            y += 40;
            for (int j = 0; j < hr; j++) {
                Wall wall = new Wall(5, 40);
                wall.setTranslateX(x);
                wall.setTranslateY(y);
                x += 40;
                walls.add(wall);
                game.getChildren().addAll(wall);
            }
            x = 200;
            for (int j = 0; j < hr - 1; j++) {

                Wall wall2 = new Wall(40, 5);
                wall2.setTranslateY(y);
                wall2.setTranslateX(x);
                x += 40;
                walls.add(wall2);
                game.getChildren().addAll(wall2);
            }


        }
        return game;
    }

    //создание рейтинга
    public Parent reytingV() {
        Text text = new Text("Reyting");
        text.setFill(Color.BLACK);
        text.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        text.setTranslateX(300);
        text.setTranslateY(100);

        HBox hBox = new HBox(200);
        hBox.setTranslateX(100);
        hBox.setTranslateY(150);

        VBox numberPlayers = new VBox(50);
        numberPlayers.setTranslateX(100);
        numberPlayers.setTranslateY(200);

        VBox names = new VBox(50);
        names.setTranslateX(200);
        names.setTranslateX(200);

        Label label1 = new Label();
        Label label2 = new Label();
        Label label3 = new Label();
        Label label4 = new Label();
        Label label5 = new Label();

        Text number1 = new Text("1.");
        number1.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Text number2 = new Text("2.");
        number2.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Text number3 = new Text("3.");
        number3.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Text number4 = new Text("4.");
        number4.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Text number5 = new Text("5.");
        number5.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        Text nomer = new Text("№");
        nomer.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Text name = new Text("Name");
        name.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        Text time = new Text("Time");
        time.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        hBox.getChildren().addAll(nomer, name, time);
        numberPlayers.getChildren().addAll(number1, number2, number3, number4, number5);
        names.getChildren().addAll(label1, label2, label3, label4, label5);

        reyting.getChildren().addAll(text, hBox, numberPlayers);

        return reyting;
    }


    @Override
    public void start(Stage primalistage) throws Exception {

        //Главное окно
        primalistage.setTitle("Game");
        primalistage.setWidth(800);
        primalistage.setHeight(800);

        Image image = new Image(getClass().getResourceAsStream("tunk.jpg"));
        ImageView img = new ImageView(image);
        img.setFitHeight(800);
        img.setFitWidth(800);

        //Кнопки начального экрана
        MenuItem btnNG = new MenuItem("New Game");

        MenuItem btnR = new MenuItem("Rating");
        MenuItem btnEX = new MenuItem("Exit");
        SubMenu mainMenu = new SubMenu(btnNG, btnR, btnEX);

        MenuItem hard = new MenuItem("Hard");
        MenuItem normal = new MenuItem("Normal");
        MenuItem easily = new MenuItem("Easily");
        MenuItem back = new MenuItem("Back");
        SubMenu gameMenu = new SubMenu(hard, normal, easily, back);

        Button btnMM1 = new Button("Main Menu");
        Button btnMM2 = new Button("Main Menu");

        //Размеры кнопок
        btnNG.setPrefSize(300, 50);
        btnR.setPrefSize(300, 50);
        btnEX.setPrefSize(300, 50);

        btnMM1.setPrefSize(100, 20);
        btnMM1.setTranslateX(300);
        btnMM1.setTranslateY(700);
        btnMM2.setPrefSize(100, 20);
        btnMM2.setTranslateX(300);
        btnMM2.setTranslateY(700);

        MenuItem btnMM = new MenuItem("Main Menu");
        btnMM.setTranslateX(200);
        btnMM.setTranslateY(600);

        Scene sceneRoot = new Scene(root);
        Scene sceneReyting = new Scene(reyting);
        Scene sceneGame = new Scene(game);

        //считывание нажатия кнопки
        sceneGame.setOnKeyPressed(event -> keys.put(event.getCode(), true));
        sceneGame.setOnKeyReleased(event -> {
            keys.put(event.getCode(), false);
        });


        MenuBox menuBox = new MenuBox(mainMenu);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
            }
        };


        //Обработчик кнопок
        btnNG.setOnMouseClicked(event -> {
            menuBox.setSubMenu(gameMenu);

        });

        back.setOnMouseClicked(event -> menuBox.setSubMenu(mainMenu));


        btnR.setOnMouseClicked(event -> {
            primalistage.setScene(sceneReyting);
        });

        btnEX.setOnMouseClicked(event -> {
            System.exit(0);
        });

        normal.setOnMouseClicked(event -> {
            primalistage.setScene(sceneGame);
            genLabirint(10);
        });

        easily.setOnMouseClicked(event -> {
            primalistage.setScene(sceneGame);
            genLabirint(5);
        });

        hard.setOnMouseClicked(event -> {
            primalistage.setScene(sceneGame);
            genLabirint(15);
        });

        btnMM1.setOnAction(event -> primalistage.setScene(sceneRoot));
        btnMM.setOnMouseClicked(event -> primalistage.setScene(sceneRoot));

        end.setTranslateX(700);


        //Добавление элементов на панель root
        root.getChildren().addAll(img, menuBox);

        //Добавление элементов на панель game
        game.getChildren().addAll(btnMM1, player, end);

        //Добавление элементов на панель Reyting
        reytingV();
        reyting.getChildren().addAll(btnMM);

        //заруск программы
        timer.start();
        primalistage.setScene(sceneRoot);
        primalistage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
